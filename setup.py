from setuptools import setup, find_packages
setup(
        name='yaslackmessenger',
        packages=find_packages(),
        version='0.0.2',
        description='Yet Another Slack Messenger',
        author='Cristiano W. Araujo',
        author_email='cristianowerneraraujo@gmail.com',
        url='https://gitlab.com/cristianowa/yaslackmessenger',
        download_url='https://gitlab.com/cristianowa/yaslackmessenger/-/archive/0.0.2/yaslackmessenger-0.0.2.zip',
        keywords=['slack', 'webhooks'],
        classifiers=[],
     )
