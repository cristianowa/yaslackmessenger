import unittest
import yaslackmessenger

class PostMock:
    def __init__(self, returns):
        self.returns = returns
        self.index = 0
        self.calls = []

    def __call__(self, *args, **kwargs):
        self.calls.append((args, kwargs))
        result = self.returns[self.index]
        self.index += 1
        return result


class TestSlacker(unittest.TestCase):
    def setUp(self):
        self.postmock = PostMock(["ok"])
        yaslackmessenger.requests.post = self.postmock

    def test_slackit(self):
        test_url = "http://test.url"
        slacker = yaslackmessenger.Slacker("test", test_url)
        ans = slacker.slackit("message")
        self.assertEqual("ok", ans)
        self.assertEqual(test_url, self.postmock.calls[0][0][0])
        self.assertEqual("message", self.postmock.calls[0][1]["json"]["text"])

    def test_slackinfo(self):
        test_url = "http://test.url"
        slacker = yaslackmessenger.Slacker("test", test_url)
        ans = slacker.slackit("message", attachments=[slacker.slackinfo("info")])
        self.assertEqual("ok", ans)
        self.assertEqual(test_url, self.postmock.calls[0][0][0])
        self.assertEqual("message", self.postmock.calls[0][1]["json"]["text"])

    def test_slackerror(self):
        test_url = "http://test.url"
        slacker = yaslackmessenger.Slacker("test", test_url)
        ans = slacker.slackit("message", attachments=[slacker.slackerror("error")])
        self.assertEqual("ok", ans)
        self.assertEqual(test_url, self.postmock.calls[0][0][0])
        self.assertEqual("message", self.postmock.calls[0][1]["json"]["text"])







