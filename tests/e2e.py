import os
import unittest
import yaslackmessenger


class TestSlacker(unittest.TestCase):
    def setUp(self):
        self.url = os.environ.get("SLACK_CHANNEL")

    def test_slackit(self):
        slacker = yaslackmessenger.Slacker("test", self.url)
        ans = slacker.slackit("message")
        self.assertEqual(200, ans.status_code)

    def test_slackinfo(self):
        test_url = "http://test.url"
        slacker = yaslackmessenger.Slacker("test", self.url)
        ans = slacker.slackit("message", attachments=[slacker.slackinfo("info")])
        self.assertEqual(200, ans.status_code)

    def test_slackerror(self):
        test_url = "http://test.url"
        slacker = yaslackmessenger.Slacker("test", self.url)
        ans = slacker.slackit("message", attachments=[slacker.slackerror("error")])
        self.assertEqual(200, ans.status_code)






